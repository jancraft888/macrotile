![MacroTile Logo](./macrotile.png)

# MacroTile

Simple and fast tiling window manager for MacOS.
Based on [tmandry/Swindler](https://github.com/tmandry/Swindler).

## Building
MacroTile uses the [Swift Package Manager](https://www.swift.org/package-manager/).

 1. Build using `swift build -c release`
 2. Copy the `macrotile` and `macrotilectl` files inside `.build/ARCH/release/` to `/usr/local/bin/` (might need root privileges by using `sudo`)
 3. Confirm MacroTile is successfully installed, just run `macrotile`
 4. It will ask you to grant accessibility permissions, do it and run the `macrotile` command again.
 5. If this time it starts up, then you got it!

## Running as a daemon on start
To run MacroTile as a daemon on login, you can create the following file
`~/Library/LaunchAgents/es.jdevstudios.macrotile.plist` and inside you must write this:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
	<dict>
		<key>Label</key>
		<string>es.jdevstudios.macrotile</string>
		<key>Program</key>
    <string>/usr/local/bin/macrotile</string>
		<key>RunAtLoad</key>
		<true/>
	</dict>
</plist>
```
Then, you can use `launchctl` to load, enable and start MacroTile:
```shell
launchctl load ~/Library/LaunchAgents/es.jdevstudios.macrotile.plist
sudo launchctl enable user/$USER/es.jdevstudios.macrotile
launchctl start ed.jdevstudios.macrotile
```

## SKHD
It is recommened to install [skhd](https://github.com/koekeishiya/skhd) to manage MacroTile from the keyboard (and allow the `BSP` option to be enabled)

Here you have a simple `skhdrc` file:
```
lctrl + alt + cmd - left : /usr/local/bin/macrotilectl move w
lctrl + alt + cmd - right : /usr/local/bin/macrotilectl move e
lctrl + alt + cmd - up : /usr/local/bin/macrotilectl move n
lctrl + alt + cmd - down : /usr/local/bin/macrotilectl move s

lctrl + alt + cmd - j : /usr/local/bin/macrotilectl size w
lctrl + alt + cmd - l : /usr/local/bin/macrotilectl size e
lctrl + alt + cmd - i : /usr/local/bin/macrotilectl size n
lctrl + alt + cmd - k : /usr/local/bin/macrotilectl size s
```

## Configuration
The config file can be found by running `macrotilectl config`

```ini
GRID_WIDTH = 4       # how many horizontal cells
GRID_HEIGHT = 2      # how many vertical cells
WIN_SIZE_RATIO = 1.5 # (recommened to keep default) used to discard small windows
GRID_GAP = 10        # gap between windows
BSP = 0              # (1) enables very simple window movement shortcuts
MENU_BAR = 0         # the pixels you want to reserve for a menu bar
```
