import Foundation
import Procbridge

enum PBError : Error {
    case serverError(String)
    case clientError(String)
}

func printhelp() {
    print("usage: macrotilectl <command> [<options>]")
    print("commands:")
    print("  move <n/s/w/e>")
    print("  size <n/s/w/e>")
    print("  config")
    print("  apps")
    print("  focus")
    exit(1)
}

if CommandLine.arguments.count < 2 {
    printhelp()
}

let port:UInt16 = 11014
do {
    let client = PBClient(host: "127.0.0.1", port: port)

    if CommandLine.arguments[1] == "move" && CommandLine.arguments.count == 3 {
        print(try client.request(method: "move", payload: CommandLine.arguments[2]))
    } else if CommandLine.arguments[1] == "size" && CommandLine.arguments.count == 3 {
        print(try client.request(method: "size", payload: CommandLine.arguments[2]))
    } else if CommandLine.arguments[1] == "config" {
        print(try client.request(method: "config", payload: 0))
    } else if CommandLine.arguments[1] == "apps" {
        var len: Int = try client.request(method: "appslen", payload: 0) as! Int
        for i in 0...(len-1) {
            print(try client.request(method: "appsidx", payload: i))
        }
    } else if CommandLine.arguments[1] == "focus" {
        print(try client.request(method: "appsfoc", payload: 0))
    } else {
        printhelp()
    }
} catch {
    print("error sending message to server. is macrotile running?")
    exit(2)
}
