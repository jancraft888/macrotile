import AXSwift
import Cocoa
import Swindler
import PromiseKit
import Procbridge
import INIParser

var globalState: Swindler.State!
var globalWindows: [Swindler.Window] = []
var winSpaceMapping: [Swindler.Window: Int] = [:]

// these two must be separated for different screens/spaces
var GRID_WIDTH = 4
var GRID_HEIGHT = 2
var WIN_SIZE_RATIO = 1.5
var GRID_GAP = 0
var MENU_BAR = 0
var BSP = 0

var screenGrids: [[CGRect]] = []
var screenMapping: [Screen: Int] = [:]

func setupGrid(screenIndex: Int, rect: CGRect) {
    let SQ_W = rect.width / Double(GRID_WIDTH)
    let SQ_H = (rect.height - Double(MENU_BAR)) / Double(GRID_HEIGHT)
    for x in 1...GRID_WIDTH {
        for y in 1...GRID_HEIGHT {
            let rect = CGRect(
                x: Double(x-1) * SQ_W + Double(GRID_GAP),
                y: Double(y-1) * SQ_H + Double(GRID_GAP),
                width: SQ_W - Double(GRID_GAP) * 2,
                height: SQ_H - Double(GRID_GAP) * 2
            )
            screenGrids[screenIndex].append(rect)
            print(rect)
        }
    }
}

func gridFindBest(screenIndex: Int, rect: CGRect, relX: Double = 0.0, relY: Double = 0.0) -> CGRect {
    let gridSq = screenGrids[screenIndex][0]
    let SQ_W = gridSq.width
    let SQ_H = gridSq.height

    if rect.width < SQ_W / WIN_SIZE_RATIO || rect.height < SQ_H / WIN_SIZE_RATIO {
        return rect
    }

    var low = rect
    var lowdst = 999999999.0
    for grid in screenGrids[screenIndex] {
        let dst = distance(a: rect.origin, b: grid.origin, relX: relX, relY: relY)
        if dst < lowdst {
            lowdst = dst
            low = grid
        }
    }

    var awsq = (rect.width / SQ_W).rounded(.toNearestOrAwayFromZero)
    var ahsq = (rect.height / SQ_H).rounded(.toNearestOrAwayFromZero)
    var rw = awsq * SQ_W + (awsq - 1) * Double(GRID_GAP)*2
    var rh = ahsq * SQ_H + (ahsq - 1) * Double(GRID_GAP)*2
    low.size.width = rw
    low.size.height = rh

    return low
}

func performBSP(screenIndex: Int, orig: CGRect, target: CGRect, frame: WriteableProperty<OfType<CGRect>>) -> CGRect {
    let gridSq = screenGrids[screenIndex][0]
    let SQ_W = gridSq.width
    let SQ_H = gridSq.height

    // 1st step, find the given window
    var ourwin: Window? = nil
    for window in globalState!.knownWindows {
        if window.frame.value == frame.value {
            ourwin = window
        }
    }

    // 2nd step, find window that intersects (visible + same screen/space)
    for window in globalState!.knownWindows {
        if window == ourwin { continue }
        if window.screen! != ourwin!.screen! { continue }
        if window.isMinimized.value { continue }
        if window.application.isHidden.value { continue }
        if winSpaceMapping[window] != ourwin!.screen!.spaceId { continue }
        if window.frame.value.width < SQ_W / WIN_SIZE_RATIO || window.frame.value.height < SQ_H / WIN_SIZE_RATIO { continue }
        if window.frame.value.intersects(target) {
            print("found: \(window)")
            frame.set(window.frame.value)
            window.frame.set(orig)
            return window.frame.value
        }
    }
    return target
}

func snapToGridRelative(screenIndex: Int, frame: WriteableProperty<OfType<CGRect>>, relX: Double, relY: Double) {
    let gridSq = screenGrids[screenIndex][0]
    let SQ_W = gridSq.width
    let SQ_H = gridSq.height

    var rX = relX
    var rY = relY
    if frame.value.origin.x + relX * SQ_W < 0 {
        rX = 0
    }
    if frame.value.origin.x + frame.value.size.width + relX * SQ_W > screenGrids[screenIndex][screenGrids[screenIndex].count - 1].maxX {
        rX = 0
    }

    let orig = CGRect(origin: CGPoint(x: frame.value.origin.x, y: frame.value.origin.y), size: CGSize(width: frame.value.size.width, height: frame.value.size.height))
    let target = gridFindBest(screenIndex: screenIndex, rect: frame.value, relX: rX * SQ_W, relY: rY * SQ_H)
    frame.set(target)
    if BSP == 1 {
        let target2 = performBSP(screenIndex: screenIndex, orig: orig, target: target, frame: frame)
        let best = gridFindBest(screenIndex: screenIndex, rect: target2)
        frame.set(best)
    }
}

func resizeToGridRelative(screenIndex: Int, frame: WriteableProperty<OfType<CGRect>>, relX: Double, relY: Double) {
    let gridSq = screenGrids[screenIndex][0]
    let SQ_W = gridSq.width
    let SQ_H = gridSq.height

    var w = frame.value.width + (SQ_W * relX)
    var h = frame.value.height + (SQ_H * relY)
    var chg = 0
    if w <= SQ_W / 4 { w = SQ_W; chg += 1 }
    if h <= SQ_H / 4 { h = SQ_H; chg += 1 }
    if chg == 2 { return }
    if frame.value.origin.x + w > screenGrids[screenIndex][screenGrids[screenIndex].count - 1].maxX {
        w -= SQ_W * relX
    }
    if frame.value.origin.y + h - SQ_H > screenGrids[screenIndex][screenGrids[screenIndex].count - 1].maxY {
        h -= SQ_H * relY
    }
    let rect = CGRect( origin: frame.value.origin, size: CGSize(width: w, height: h) )

    if h < frame.value.height {
        let target = gridFindBest(screenIndex: screenIndex, rect: rect, relY: 1.0 * SQ_H)
        frame.set(target)
    } else {
        let target = gridFindBest(screenIndex: screenIndex, rect: rect)
        frame.set(target)
    }
}

func distance(a: CGPoint, b: CGPoint, relX: Double = 0.0, relY: Double = 0.0) -> Double {
    let x = (a.x + relX) - b.x
    let y = (a.y + relY) - b.y
    return (x * x + y * y).squareRoot()
}

func dispatchAfter(delay: TimeInterval, block: DispatchWorkItem) {
    let time = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: time, execute: block)
}

// proccess control
let port:UInt16 = 11014
let server = PBServer(port : port) { (method,args) in
    switch method {
    case "move":
        let dir = (args as! String).lowercased()
        if let scr = globalState?.frontmostApplication.value?.focusedWindow.value?.screen {
            if let win = globalState?.frontmostApplication.value?.focusedWindow.value {
                if dir == "n" {
                    snapToGridRelative(screenIndex: screenMapping[scr] ?? -1, frame: win.frame, relX: 0.0, relY: 1.0)
                    return "moving north"
                } else if (dir == "s") {
                    snapToGridRelative(screenIndex: screenMapping[scr] ?? -1, frame: win.frame, relX: 0.0, relY: -1.0)
                    return "moving south"
                } else if (dir == "w") {
                    snapToGridRelative(screenIndex: screenMapping[scr] ?? -1, frame: win.frame, relX: -1.0, relY: 0.0)
                    return "moving west"
                } else if (dir == "e") {
                    snapToGridRelative(screenIndex: screenMapping[scr] ?? -1, frame: win.frame, relX: 1.0, relY: 0.0)
                    return "moving east"
                }
                return "unknown direction"
            }
        }
        return "window not found"
    case "size":
        let dir = (args as! String).lowercased()
        if let scr = globalState?.frontmostApplication.value?.focusedWindow.value?.screen {
            if let win = globalState?.frontmostApplication.value?.focusedWindow.value {
                if dir == "n" {
                    resizeToGridRelative(screenIndex: screenMapping[scr] ?? -1, frame: win.frame, relX: 0.0, relY: -1.0)
                    return "resizing smaller (north)"
                } else if (dir == "s") {
                    resizeToGridRelative(screenIndex: screenMapping[scr] ?? -1, frame: win.frame, relX: 0.0, relY: 1.0)
                    return "resizing bigger (south)"
                } else if (dir == "w") {
                    resizeToGridRelative(screenIndex: screenMapping[scr] ?? -1, frame: win.frame, relX: -1.0, relY: 0.0)
                    return "resizing smaller (west)"
                } else if (dir == "e") {
                    resizeToGridRelative(screenIndex: screenMapping[scr] ?? -1, frame: win.frame, relX: 1.0, relY: 0.0)
                    return "resizing bigger (east)"
                }
                return "unknown direction"
            }
        }
        return "window not found"
    case "config":
        return "\(FileManager.default.homeDirectoryForCurrentUser.path)/.config/macrotile/config.ini"
    case "appslen":
        var cnt = 0
        for app in globalState!.runningApplications {
            if let bid = app.bundleIdentifier {
                if let title = app.focusedWindow.value?.title.value {
                    cnt += 1
                }
            }
        }
        return cnt
    case "appsidx":
        let idx = args as! Int
        var cnt = 0
        for app in globalState!.runningApplications {
            if let bid = app.bundleIdentifier {
                if let title = app.focusedWindow.value?.title.value {
                    cnt += 1
                    if cnt - 1 != idx { continue }
                    return "\(bid) \(title)"
                }
            }
        }
        return "index not found"
    case "appsfoc":
        if let app = globalState?.frontmostApplication.value {
            if let bid = app.bundleIdentifier {
                if var title = app.focusedWindow.value?.title.value {
                    return "\(bid) \(title)"
                }
            }
        }
        return "no focused app found"
    case "err":
        print("macrotile server error \(args)")
        return 0
    default:
        return "the command specified is not implemented in the macrotile server"
    }
}

class AppDelegate: NSObject, NSApplicationDelegate {
    var swindler: Swindler.State!

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        guard AXSwift.checkIsProcessTrusted(prompt: true) else {
            print("Not trusted as an AX process; please authorize and re-launch")
            NSApp.terminate(self)
            return
        }

        Swindler.initialize().done { state in
            globalState = state
            self.swindler = state

            if let ini = try? INIParser("\(FileManager.default.homeDirectoryForCurrentUser.path)/.config/macrotile/config.ini") {
                if let defGW = ini.anonymousSection["GRID_WIDTH"] {
                    GRID_WIDTH = Int(defGW) ?? GRID_WIDTH
                }
                if let defGH = ini.anonymousSection["GRID_HEIGHT"] {
                    GRID_HEIGHT = Int(defGH) ?? GRID_HEIGHT
                }
                if let defWSR = ini.anonymousSection["WIN_SIZE_RATIO"] {
                    WIN_SIZE_RATIO = Double(defWSR) ?? WIN_SIZE_RATIO
                }
                if let defGG = ini.anonymousSection["GRID_GAP"] {
                    GRID_GAP = Int(defGG) ?? GRID_GAP
                }
                if let defBSP = ini.anonymousSection["BSP"] {
                    BSP = Int(defBSP) ?? BSP
                }
                if let defMB = ini.anonymousSection["MENU_BAR"] {
                    MENU_BAR = Int(defMB) ?? MENU_BAR
                }
                print("config loaded from file")
            }

            self.setupEventHandlers()
            server.start()
        }.catch { error in
            print("Fatal error: failed to initialize Swindler: \(error)")
            NSApp.terminate(self)
        }

        //    dispatchAfter(10.0) {
        //      for window in self.swindler.knownWindows {
        //        let title = window.title.value
        //        print("resizing \(title)")
        //        window.size.set(CGSize(width: 200, height: 200)).done { newValue in
        //          print("done with \(title), valid: \(window.isValid), newValue: \(newValue)")
        //        }.error { error in
        //          print("failed to resize \(title), valid: \(window.isValid), error: \(error)")
        //        }
        //      }
        //    }
    }

    private func setupEventHandlers() {
        print("screens: \(swindler.screens)")
        var index = 0
        for scr in swindler.screens {
            screenMapping[scr] = index
            screenGrids.append([])
            setupGrid(screenIndex: index, rect: swindler.screens[index].applicationFrame)
            index += 1
        }
        for window in swindler.knownWindows {
            winSpaceMapping[window] = window.screen!.spaceId
            print("registered \(window) as \(window.screen!.spaceId)")
        }

        swindler.on { (event: WindowCreatedEvent) in
            let window = event.window

            if let scr = window.screen {
                window.frame.set(gridFindBest(screenIndex: screenMapping[scr] ?? -1, rect: window.frame.value))
            }

            print("new window: \(window.title.value)")
            winSpaceMapping[window] = window.screen!.spaceId
            print("registered \(window) as \(window.screen!.spaceId)")
        }
        swindler.on { (event: WindowFrameChangedEvent) in
            if !event.external { return }
            if let scr = event.window.screen {
                event.window.frame.set(gridFindBest(screenIndex: screenMapping[scr] ?? -1, rect: event.window.frame.value))
            }
            print("Frame changed from \(event.oldValue) to \(event.newValue),",
                  "external: \(event.external)")
        }
        swindler.on { (event: WindowDestroyedEvent) in
            print("window destroyed: \(event.window.title.value)")
        }
        swindler.on { (event: ApplicationMainWindowChangedEvent) in
            print("new main window: \(String(describing: event.newValue?.title.value)).",
                  "[old: \(String(describing: event.oldValue?.title.value))]")
            self.frontmostWindowChanged()
        }
        swindler.on { (event: FrontmostApplicationChangedEvent) in
            print("new frontmost app: \(event.newValue?.bundleIdentifier ?? "unknown").",
                  "[old: \(event.oldValue?.bundleIdentifier ?? "unknown")]")
            self.frontmostWindowChanged()
        }
        swindler.on { (event: ScreenLayoutChangedEvent) in
            if !event.external { return }
            print("addscr \(event.addedScreens)")
            print("rmscr \(event.removedScreens)")
            print("chscr \(event.changedScreens)")
        }
        swindler.on { (event: SpaceDidChangeEvent) in
            if !event.external { return }
            for window in self.swindler.knownWindows {
                if winSpaceMapping[window] == nil {
                    print("registered \(window) as \(window.screen!.spaceId)")
                    winSpaceMapping[window] = window.screen!.spaceId
                }
            }
        }
    }

    private func frontmostWindowChanged() {
        if let window = swindler.frontmostApplication.value?.mainWindow.value {
            print("new frontmost window: \(String(describing: window.title.value))")
            if let scr = window.screen {
                window.frame.set(gridFindBest(screenIndex: screenMapping[scr] ?? -1, rect: window.frame.value))
            }
        } else {
            print("new frontmost window: nil")
        }
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
}

